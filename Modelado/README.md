## 4 Modelado

En esta sección se presenta el modelado orientado a grafos, las entidades y relaciones, así como para cada nodo el conjunto de propiedades en claves-valor.

### 4.1 Entidades/Objetos

Las entidades con las que se cuentan son:

1. Model
2. Metric
3. Image
4. LearningTransferModel

### 4.2 Relaciones

![](diagramas/mapa.jpg)

- Existe una relación "Uno a muchos" de Modelo a Métrica. Un modelo puede generar varias métricas.
- Existe una relación de "Uno a muchos" de Modelo a Imagen, donde un modelo requiere de muchas imágenes para el entrenamiento.
- Existe una relación de "Uno a uno" de Modelo a Modelo pre-entrenado, ya que solo puede tomar un solo checkpoint para hacer la transferencia de aprendizaje.

### 4.3 Consultas

**¿Qué información se requiere recuperar?**

La información que más se va a consultar es el _tipo de modelo y la información de las métricas_.

- No se requiere acceder algún campo especifico del archivo de configuración del modelo.

**Consultas**

- **Necesitamos poder filtrar las métricas de acuerdo a un tipo de modelo** Por ejemplo, obtener la métrica de _Average precision_ para el modelo Faster R-CNN.

- **Necesitamos poder filtrar métricas obtenidas para un determinado conjunto de datos para un determinado número de pasos**

### 4.4 Prototipo Grafo

Prototipando grafos enfocados a la solución se tiene el siguiente diagrama:

![](diagramas/Modelado_Nodos.jpg)

Como se puede observar se pueden tener varios modelos, cada modelo tiene un modelo de transferencia del cual toma el aprendizaje de un checkpoint y cada modelo genera un conjunto de métricas.

Nosotros nos enfocamos en esta implementación en **poder conocer y acceder a las métricas que se obtienen para cada modelo, para determinado conjunto de imágenes**.
Por ahora lo que se tiene es el experimento para el model1 el cual es: FasterRCNN_ResNet101 para 1036 pasos, para el conjunto de datos CIMAT-CyclistV1, sin embargo, se pretende probar nuevos modelos y se esta trabajando en la obtención de otra base de datos de ciclistas tomados de las calles de la ciudad.

#### 4.4.1 Relaciones

La fecha en las relaciones  nos sirve para conocer las versiones de datos entrenados correspondientes a cada modelo.

**Generates_As_A_Result**

Esta relación es representa que cada modelo genera métricas.

Contiene el parámetro de fecha en el cuál se calcularon las métricas.

```
{tipo: "Model"}-[:Generates_As_A_Result]-> {tipo: "Metric"}
```

**Is_Processed_By** 

Esta relación representa que el modelo procesa la información de las imágenes.
Contiene el parámetro de fecha en el cuál se entrenó el modelo tomando las imágenes.

```
{tipo: "Model" } <- [:Is_Processed_By]-{tipo: "Image"}
```

**Transfer_Learning**

Esta relación representa que el modelo pre-entrenado transfiere el aprendizaje a un determinado modelo.

```
{ tipo: "Model" } <- [:Transfer_Learning]-{tipo: "LearningTransferModel""}
```

#### 4.4.2 Nodos

**Establecer propiedades del Nodo**

Se ha decidido solo almacenar las rutas donde se encuentran los archivos tanto binarios como serializados (.ckpt, .pb, .record), ya que como tal no se hará consultas a los mismos, sino solo para conocer cuáles fueron utilizados para obtener las métricas, por ejemplo, además de mantener la relación que existen entre todos los nodos.
Todas las rutas comparten el directorio 'C:/tensorflow_ciclist/models/research/object_detection'

**Atributos del nodo Model**

Se ha decidido almacenar información general del modelo, la cuál nos servirá para conocer _Qué modelo_ fue el que generó _tal métrica_.

Se incluye el nombre, arquitectura, extracción de características, pasos de entrenamiento, y las rutas del archivo de configuración, el punto de control y resultado de inferencia .pb generado.

```json
{
    "name":"Model1",
    "architecture":"FasterRCNN",
    "featureExtractor":"ResNet101",
    "steps":1036,
    "model_config":"/inference_graph/pipeline.config",
    "model_checkpoint_path": "/inference_graph/model.ckpt",
    "tf_model_path":"/inference_graph/frozen_inference_graph.pb"
}
```

**Atributos del nodo LearningTransferModel**

Se almacena de manera general también la información correspondiente al modelo obtenido de: _Tensorflow detection model zoo_

```json
{
    "name":"TrainModel1",
    "architecture":"Faster_rcnn",
    "featureExtractor":"ResNet101",
    "imageDataset":"COCO",
    "model_checkpoint_path": "/faster_rcnn_resnet101_coco_2018_01_28/model.ckpt",
    "tf_model_path":"/faster_rcnn_resnet101_coco_2018_01_28/frozen_inference_graph.pb"
}
```

**Atributos del nodo Metric**

Cuenta con la información de las métricas proporcionadas por el _API de COCO metrics_.

|Métrica    | Descripción |
| ---------- | ---------- |
| AP  | Average Precision. Siendo la precision: "Cuando nuestro modelo predice que es un ciclista, acierta el _% de las veces" |
| AP_small, AP_medium y AP_large   | Average Precision Across Scales para imágenes pequeñas, medianas y grandes |
| ARmax1, ARmax10, ARmax100  | Average Recall para 1, 10 y 100 imágenes. Siendo el recall: "Nuestro modelo identifica correctamente el _% de los ciclistas" |
| AR_small, AR_medium y AR_large  | Average Recall Across Scales para imágenes pequeñas, medianas y grandes  |
| mAP  |  mean Average Precision enfocado a la detección de los cuadros delimitadores (Detection Boxes)  |
| mAP_large, mAP_medium, mAP_small | mean Average Precision para imágenes grandes, medianas y pequeñas   |
| mAP_50,mAP_75  | mean Average Precision para un umbral de 50 y 75|
| AR_1 al AR_100_small  | Average Recall enfocado a la detección del cuadro delimitador (Detection Boxes)   |
| classification_loss  | Pérdida para la clasificación de objetos detectados de la clase ciclista |
| localization_loss  |Pérdida de localización o pérdida del regresor del cuadro delimitador (BoxClassifierLoss)   |
| localization_loss_RPN  |Pérdida de localización o el regresor de pérdida de la caja delimitadora para el RPN  |
| objectness_loss  | Pérdida del clasificador que clasifica si un cuadro delimitador es un objeto de interés o de fondo|

```json
{
    "name":"MetricExp1",
    "api":"COCO API",
    "AP":0.787,
    "AP_iou_50":0.968,
    "Ap_iou_75":0.923,
    "AP_small":-1.0,
    "AP_medium":0.5, 
    "AP_large":0.803,
    "ARmax1":0.366,
    "ARmax10":0.832,
    "ARmax100":0.833,
    "AR_small":-1.0,
    "AR_medium":0.659,
    "AR_large":0.844,
    "mAP":0.787278,
    "mAP_large":0.802791,
    "mAP_medium":0.500373,
    "mAP_small":-1.0,
    "mAP_50":0.968427,
    "mAP_75":0.923265,
    "AR_1":0.36512,
    "AR_10":0.831624,
    "AR_100":0.832821,
    "AR_100_large":0.8443557,
    "AR_100_medium":0.658824,
    "AR_100_small":-1.00,
    "classification_loss":0.088030,
    "localization_loss":0.041369,
    "localization_loss":0.226560,
    "objectness_loss":0.106355
}
```

**Atributos del nodo Image**

Inicialmente se pensaba en almacenar dentro del nodo toda la información de cada imagen, sin embargo, como es información que como tal no se iba a estar consultando se ha considerado una mejor manera al solo tomar la información útil para conocer las condiciones del conjunto de imágenes con los cuales se relacionan con las métricas y modelos.

El modelado quedo de la siguiente manera:

```json
{
    "name":"CIMAT-CyclistV1",
    "contributor":"CIMAT Zacatecas",
    "year":2019,
    "class":"ciclista",
    "instances":2866,
    "images":1336,
    "image_train":1096,
    "image_test":240,
    "images_train_path":"/images/1/train",
    "images_test_path":"/images/1/test"
}
```