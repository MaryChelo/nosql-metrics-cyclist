# NoSQL-Metrics-Cyclist


En la actualidad el desarrollo de nuevas tecnologías y aplicaciones ha permitido la evolución en la forma en cómo se guardan los datos, aumentando el auge del movimiento NoSQL (del inglés, Not Only SQL) principalmente por la exigencia de aspectos como la alta concurrencia en la lectura, escritura con alta latencia, almacenamiento eficiente de grandes volúmenes de datos, alta escalabilidad, alta disponibilidad menores costos operativos y de gestión (Jing Han, Haihong E, Guan Le, y Jian Du, 2011). El almacenamiento utilizando NoSQL se puede dividir en clave-valor, orientado
a documentos, orientado a columnas, orientado a grafos y multimodelos.

Las bases de datos orientados a grafos están especializadas en la gestión eficiente de datos fuertemente vinculados y son más adecuadas para las aplicaciones basadas en datos que involucran muchas relaciones.

Los nodos y las aristas consisten en objetos de claves y valores que se pueden definir en un esquema y éstas proporcionan un recorrido de nodo de alto rendimiento y recuperación de datos.

Se realizó el modelado de la base de datos orientado a grafos debido a la importancia de las relaciones entre las entidades que hemos establecido: Modelo, Métrica, Imagen, y Modelo pre-entrenado.

Existe una relación de uno a muchos de modelo a métrica, debido a que un modelo puede generar varias métricas, por otro lado un modelo procesa un conjunto de datos para el entrenamiento y solo usa un modelo pre-entrenado para el aprendizaje por transferencia.

El objetivo principal de almacenar las métricas es realizar la comparación de los diferentes modelos. El modelado de la base de datos que se realizó nos proporciona las métricas, qué modelo lo obtuvo y con qué conjunto de datos fue entrenado.
Actualmente existen varias tecnologías que soportan este tipo de base de datos, entre las que se encuentran, Ne04j, Microsoft Azure Cosmos DB, OrientDB, ArangoDB, Virtuoso y GraphDB (Curator, 2019).

Para el trabajo de tesis se realizó la implementación de la base de datos orientada a grafos utilizando Neo4J, dicha tecnología permite recuperar los datos en forma de consultas, escritas en lenguaje de programación Cypher, el cual es famoso por su simplicidad al recuperar y declarar relaciones entre nodos, además Neo4J se puede integrar con Python usando la librería Py2neo, la cual es útil para escribir las consultas como declaraciones de programación (Johnpaul y Mathew, 2017).

Se realiza entonces para este trabajo de tesis, las consultas Cypher basadas en NoSQL utilizando la base de gráficos Neo4j, una de las consultas más importantes es: obtener alguna métrica en específico, así como también el modelo qué lo obtuvo y a qué conjunto de datos pertenece